import dataclasses
import typing

import kubernetes
import urwid
import urwid.util

from katie.api import Katie, KatiePodStatus


@dataclasses.dataclass()
class KatiePodsUIState:
    logs: urwid.ListWalker
    menu: urwid.ListWalker
    meta: urwid.Text

    logs_listbox: urwid.ListBox
    menu_listbox: urwid.ListBox

    logs_linebox: urwid.LineBox
    menu_linebox: urwid.LineBox
    meta_linebox: urwid.LineBox

    def __init__(self):
        self.logs = urwid.SimpleListWalker([])
        self.menu = urwid.SimpleFocusListWalker([])
        self.meta = urwid.Text(markup="No pod selected...")

        self.logs_listbox = urwid.ListBox(self.logs)
        self.logs_listbox.set_focus_valign("bottom")
        self.menu_listbox = urwid.ListBox(self.menu)

        self.logs_linebox = urwid.LineBox(self.logs_listbox, title="Container logs")
        self.menu_linebox = urwid.LineBox(self.menu_listbox, title="Pods")
        self.meta_linebox = urwid.LineBox(self.meta, title="Pod metadata")

    def add_menu_item(
        self,
        text: str,
        callback: typing.Callable,
        user_args: typing.List[typing.Any],
        attribute: typing.Optional[str] = None,
    ):
        button = urwid.Button((attribute, text))
        urwid.connect_signal(button, "click", callback, user_args=list(user_args))
        self.menu.append(urwid.AttrMap(button, None, focus_map="focused"))
        if len(self.menu) == 1:
            button._emit("click")

    def set_meta(
        self, name: str, items: typing.Sequence[typing.Tuple[str, str]]
    ) -> None:
        self.meta_linebox.set_title(f"Pod metadata ({name})")
        self.meta.set_text("\n".join(f"{k}: {v}" for k, v in items))

    def set_logs(self, name: str, log: str, warn: typing.Sequence[str]) -> None:
        self.logs_linebox.set_title(f"Container logs ({name})")
        self.logs.clear()
        for line in log.splitlines():
            self.logs.append(urwid.Text(line))
        for warning in warn:
            self.logs.append(urwid.Text(("warning", warning), align="right"))
        if len(self.logs):
            self.logs_listbox.set_focus(len(self.logs_listbox.body) - 1)

    def main(self) -> typing.NoReturn:
        columns = urwid.Columns(
            [
                ("weight", 1, self.menu_linebox),
                (
                    "weight",
                    2,
                    urwid.Pile(
                        [("pack", self.meta_linebox), ("weight", 1, self.logs_linebox)]
                    ),
                ),
            ]
        )

        loop = urwid.MainLoop(
            columns,
            palette=[
                ("focused", "standout", ""),
                ("warning", "dark red", ""),
                ("focused_warning", "standout,dark red", ""),
            ],
        )
        loop.run()


@dataclasses.dataclass()
class KatiePodsUI:
    katie: Katie
    state: KatiePodsUIState = dataclasses.field(default_factory=KatiePodsUIState)

    def main(self, pods: typing.Sequence[kubernetes.client.V1Pod]) -> typing.NoReturn:
        for pod in pods:
            self.state.add_menu_item(
                text=pod.metadata.name,
                callback=self.select_pod,
                user_args=[pod],
                attribute="focused_warning" if not self._pod_is_ready(pod) else None,
            )
        self.state.main()

    def select_pod(self, pod: kubernetes.client.V1Pod, _button: urwid) -> None:
        status: KatiePodStatus = self.katie.status(pod)
        self.state.set_meta(
            name=status.container.name,
            items=[
                ("Name", status.pod.metadata.name),
                ("Status", status.pod.status.phase),
                ("Conditions", status.conditions),
            ],
        )
        self.state.set_logs(
            name=status.container.name,
            warn=status.container_warn,
            log=status.container_log,
        )

    def _pod_is_ready(self, pod: kubernetes.client.V1Pod) -> bool:
        conditions = {c.type: c for c in pod.status.conditions}
        return conditions["Ready"].status == "True"
