import subprocess
import typing

import click.exceptions


def kubectl_exec(pod_name: str, command: typing.Sequence[str]):
    command = ("kubectl", "exec", "-it", pod_name, "--", *command)

    try:
        subprocess.run(command, check=True)
    except subprocess.CalledProcessError as error:
        raise click.exceptions.Exit(error.returncode)
