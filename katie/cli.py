import logging
import sys
import typing

import click

from . import __doc__
from .api import Katie
from .kubectl.exec import kubectl_exec
from .tui import KatiePodsUI
from .util.filter import (
    FilterAnnotations,
    FilterApp,
    FilterKinds,
    FilterLabels,
    FilterNames,
)
from .util.selectors import FieldSelector, LabelSelector, SelectorType

field_selector_option = click.option(
    "-f",
    "--field-selector",
    "field_selector",
    type=SelectorType(FieldSelector),
    required=False,
    help="Kubernetes field selector",
)
label_selector_option = click.option(
    "-s",
    "--selector",
    "--label-selector",
    "label_selector",
    type=SelectorType(LabelSelector),
    required=False,
    help="Kubernetes label selector",
)


def selectors(func: typing.Callable) -> typing.Callable:
    return field_selector_option(label_selector_option(func))


@click.group(name="katie", help=__doc__)
@click.option(
    "-v",
    "--verbose",
    "verbose",
    count=True,
    help="Increase logging detail (can be used twice).",
)
@click.pass_context
def main(ctx: click.Context, verbose: int):
    levels = {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}
    logging.basicConfig(level=levels[verbose])
    ctx.obj = Katie.from_config()


@main.command(name="ui")
@selectors
@click.pass_obj
def ui(katie: Katie, field_selector: FieldSelector, label_selector: LabelSelector):
    """List pods matching a selector."""
    pods = katie.pods(field_selector=field_selector, label_selector=label_selector)
    KatiePodsUI(katie).main(pods)


@main.command(name="pods")
@selectors
@click.pass_obj
def list_pods(
    katie: Katie, field_selector: FieldSelector, label_selector: LabelSelector
):
    """List pods matching a selector."""
    for pod in katie.pods(field_selector=field_selector, label_selector=label_selector):
        click.echo(pod.metadata.name)


@main.command(name="pod")
@selectors
@click.pass_obj
def get_pod(katie: Katie, field_selector: FieldSelector, label_selector: LabelSelector):
    """Print the first pod matching the selectors."""
    pod = katie.pod(field_selector=field_selector, label_selector=label_selector)
    click.echo(pod.metadata.name)


@main.command(name="logs")
@selectors
@click.option("-c", "--container", "container_name", type=click.STRING, default=None)
@click.pass_obj
def logs(
    katie: Katie,
    field_selector: FieldSelector,
    label_selector: LabelSelector,
    container_name: typing.Optional[str],
) -> None:
    """Get logs for a pod."""
    pod = katie.pod(field_selector=field_selector, label_selector=label_selector)
    status = katie.status(pod, container_name=container_name)
    click.echo(status.container_log.rstrip())


@main.command(name="exec")
@selectors
@click.argument("command", type=click.STRING, required=True, nargs=-1)
@click.pass_context
def exec_command(
    ctx: click.Context,
    field_selector: FieldSelector,
    label_selector: LabelSelector,
    command: typing.Sequence[str],
) -> None:
    """Get logs for a pod."""
    pod = ctx.obj.pod(field_selector=field_selector, label_selector=label_selector)
    kubectl_exec(pod.metadata.name, command)


@main.command(name="resources")
def resources(katie: Katie,) -> None:
    """Get node resource usage."""
    pass


@main.command(name="filter")
@click.option("--annotation", "annotations", multiple=True, type=click.STRING)
@click.option("--kind", "kinds", multiple=True, type=click.STRING)
@click.option("--label", "labels", multiple=True, type=click.STRING)
@click.option("--name", "names", multiple=True, type=click.STRING)
@click.option(
    "-s", "--summary/--no-summary", "summary", is_flag=True, default=False,
)
@click.argument("streams", nargs=-1, type=click.File(encoding="utf-8"))
def filter_resources(
    annotations: typing.Collection[str],
    names: typing.Collection[str],
    labels: typing.Collection[str],
    kinds: typing.Collection[str],
    streams: typing.Sequence[typing.IO],
    summary: bool,
) -> None:
    """Filter a stream of resources."""
    FilterApp().main(
        streams=streams or (sys.stdin,),
        filters=[
            FilterAnnotations(annotations),
            FilterNames(names),
            FilterLabels(labels),
            FilterKinds(kinds),
        ],
        summary=summary,
    )


@main.command(name="inspect")
@selectors
@click.argument("names", metavar="NAME...", nargs=-1, type=click.STRING)
@click.pass_obj
def openssl(
    katie: Katie,
    names: typing.Sequence[str],
    field_selector: FieldSelector,
    label_selector: LabelSelector,
) -> None:
    """
    Inspect a 'kubernetes.io/tls' Secret.
    """

    for secret in katie.secrets(
        names=names, field_selector=field_selector, label_selector=label_selector
    ):
        assert secret.type == "kubernetes.io/tls"
