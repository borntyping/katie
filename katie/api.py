import dataclasses
import logging
import os.path
import pathlib
import typing

import kubernetes.client
import kubernetes.config.kube_config
import yaml

from katie.util.selectors import FieldSelector, LabelSelector, Selector
from katie.util.types import KatieException

log = logging.getLogger(__name__)

Pods = typing.Sequence[kubernetes.client.V1Pod]


@dataclasses.dataclass()
class KatiePodStatus(object):
    pod: kubernetes.client.V1Pod
    container: kubernetes.client.V1ContainerStatus
    container_log: str

    @property
    def status(self) -> kubernetes.client.V1PodStatus:
        return self.pod.status

    @property
    def conditions(self) -> typing.Sequence[str]:
        return ", ".join(self._condition(c) for c in self.status.conditions if c)

    @staticmethod
    def _condition(condition: kubernetes.client.V1PodCondition) -> str:
        if condition.status == "True":
            return f"{condition.type}"
        elif condition.status == "False":
            return f"Not {condition.type.lower()} ({condition.message})"
        raise NotImplementedError(
            f"Unknown condition status {condition.status} for {condition}"
        )

    @property
    def container_warn(self) -> str:
        last_state: kubernetes.client.V1ContainerState = self.container.last_state
        if last_state.terminated:
            yield f"Container terminated with exit code {last_state.terminated.exit_code}"

        if self.container.state.waiting:
            yield f"{self.container.state.waiting.reason}: {self.container.state.waiting.message}"


@dataclasses.dataclass()
class Katie:
    client: kubernetes.client.CoreV1Api
    namespace: str

    def pod(
        self,
        *,
        field_selector: typing.Optional[FieldSelector],
        label_selector: typing.Optional[LabelSelector],
    ) -> kubernetes.client.V1Pod:
        selected_pods = self.pods(
            field_selector=field_selector, label_selector=label_selector
        )
        if len(selected_pods) > 1:
            log.warning("Selected multiple pods, using the first pod returned")
        return selected_pods[0]

    def pods(
        self,
        *,
        field_selector: typing.Optional[FieldSelector] = None,
        label_selector: typing.Optional[LabelSelector] = None,
    ) -> Pods:
        """
        A Selector can generate multiple variants - we search until we find results.
        """
        for field_selector in Selector.as_strings(field_selector):
            for label_selector in Selector.as_strings(label_selector):
                pods = self._list_namespaced_pod(field_selector, label_selector)
                log.info(pods)
                if pods:
                    return pods

        raise KatieException(f"Selectors matched no pods")

    def _list_namespaced_pod(
        self,
        field_selector: typing.Optional[str] = None,
        label_selector: typing.Optional[str] = None,
    ) -> Pods:
        log.info(f"Listing pods matching selectors")
        params = {}
        if field_selector is not None:
            log.info(f"Field selector={field_selector}")
            params["field_selector"] = field_selector
        if label_selector is not None:
            log.info(f"Label selector={label_selector}")
            params["label_selector"] = label_selector
        return self.client.list_namespaced_pod(self.namespace, **params).items

    def status(
        self,
        pod: kubernetes.client.V1Pod,
        container_name: typing.Optional[str] = None,
        tail_lines: typing.Optional[int] = None,
    ) -> KatiePodStatus:
        container_status = self._container_status(pod, container_name)
        container_log = self._container_log(pod, container_status, tail_lines)
        return KatiePodStatus(
            pod=pod, container=container_status, container_log=container_log
        )

    def _container_status(
        self, pod: kubernetes.client.V1Pod, name: typing.Optional[str] = None
    ) -> kubernetes.client.V1ContainerStatus:
        statuses = []

        if pod.status.init_container_statuses is not None:
            statuses.extend(pod.status.init_container_statuses)

        if pod.status.container_statuses is not None:
            statuses.extend(pod.status.container_statuses)

        if name is not None:
            status_map = {s.name: s for s in statuses}
            return status_map[name]

        if len(statuses) == 1:
            return statuses[0]

        raise NotImplementedError(
            "Multiple containers in pod, specify a container name"
        )

    def _container_log(
        self,
        pod: kubernetes.client.V1Pod,
        container_status: kubernetes.client.V1ContainerStatus,
        tail_lines: typing.Optional[int] = None,
    ) -> str:
        if pod.status.phase == "Pending":
            raise Exception("Pod is pending startup and has no logs")

        kwargs = {}

        if tail_lines is not None:
            kwargs["tail_lines"] = tail_lines

        return self.client.read_namespaced_pod_log(
            pod.metadata.name,
            pod.metadata.namespace,
            container=container_status.name,
            **kwargs,
        )

    @classmethod
    def from_config(
        cls,
        config_file: typing.Optional[str] = None,
        context: typing.Optional[str] = None,
    ):
        api_client = kubernetes.config.new_client_from_config(config_file, context)
        client = kubernetes.client.CoreV1Api(api_client)
        namespace = cls._namespace_for_context(context=context, config_file=config_file)
        return cls(client, namespace)

    @classmethod
    def _namespace_for_context(
        cls,
        *,
        config_file: typing.Optional[str] = None,
        context: typing.Optional[str] = None,
    ) -> str:
        """
        The configuration loaded from the Kubernetes client ignores the namespace for
        each context, so we load the config file again and work it out ourselves. Sorry.
        """
        default_config_file = kubernetes.config.kube_config.KUBE_CONFIG_DEFAULT_LOCATION
        config_file = config_file or os.path.expanduser(default_config_file)
        config = yaml.safe_load(pathlib.Path(config_file).read_text())

        current_context = context or config.get("current-context")
        if not current_context:
            raise KatieException(
                "No context set and no current-context in configuration"
            )

        contexts = {c["name"]: c["context"] for c in config["contexts"]}
        return contexts[current_context]["namespace"]

    def secrets(
        self,
        names: typing.Sequence[str],
        field_selector: FieldSelector,
        label_selector: LabelSelector,
    ) -> typing.Sequence[kubernetes.client.V1Secret]:
        if names and (field_selector or label_selector):
            raise Exception("Can't specify names and selectors.")

        if names:
            return [
                self.client.read_namespaced_secret(name=name, namespace=self.namespace)
                for name in names
            ]

        return [
            self.client.list_namespaced_secret(
                namespace=self.namespace,
                field_selector=field_selector_str,
                label_selector=label_selector_str,
            )
            for field_selector_str in Selector.as_strings(field_selector)
            for label_selector_str in Selector.as_strings(label_selector)
        ]
