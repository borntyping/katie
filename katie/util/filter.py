import abc
import dataclasses
import typing

import click
import yaml

YAML = typing.Any


@dataclasses.dataclass()
class Filter(typing.Callable[[YAML], bool], metaclass=abc.ABCMeta):
    matches: typing.Collection[str]

    def __call__(self, document: YAML) -> bool:
        if not self.matches:
            return True

        matches = {string.casefold() for string in self.matches}
        values = {v.casefold() for v in self.values(document) if isinstance(v, str)}
        return any(m in v for v in values for m in matches)

    @abc.abstractmethod
    def values(self, document: YAML) -> typing.Collection[str]:
        raise NotImplementedError


@dataclasses.dataclass()
class FilterAnnotations(Filter):
    def values(self, document: YAML) -> typing.Collection[str]:
        return document.get("metadata", {}).get("annotations", {})


@dataclasses.dataclass()
class FilterNames(Filter):
    def values(self, document: YAML) -> typing.Collection[str]:
        return [document.get("metadata", {}).get("name")]


@dataclasses.dataclass()
class FilterLabels(Filter):
    def values(self, document: YAML) -> typing.Collection[str]:
        return document.get("metadata", {}).get("labels", {})


@dataclasses.dataclass()
class FilterKinds(Filter):
    def values(self, document: YAML) -> typing.Collection[str]:
        return [document.get("kind")]


class FilterApp:
    def main(
        self,
        streams: typing.Iterable[typing.IO],
        filters: typing.Collection[Filter],
        summary: bool = False,
    ) -> None:
        documents = self.filter_documents(self.load_all(streams), filters)

        if summary:
            documents = [[self.summary(document) for document in documents]]

        click.echo(yaml.dump_all(documents), nl=False)

    @staticmethod
    def summary(document: typing.Mapping) -> typing.Mapping[str, typing.Optional[str]]:
        return {
            "kind": document.get("kind"),
            "name": document.get("metadata", {}).get("name"),
        }

    @staticmethod
    def filter_documents(
        documents: typing.Iterable[YAML], filters: typing.Collection[Filter]
    ) -> typing.Iterable[YAML]:
        for document in documents:
            if all(f(document) for f in filters):
                yield document

    @staticmethod
    def load_all(streams: typing.Iterable[typing.IO]) -> typing.Iterable[YAML]:
        for stream in streams:
            for document in yaml.safe_load_all(stream):
                yield document
