import dataclasses
import re
import typing

import click

from katie.util.types import KatieException


@dataclasses.dataclass()
class Requirement:
    label: str
    operator: str
    value: str

    regex = re.compile("(?P<label>.+)(?P<operator>=|==|!=)(?P<value>.+)")

    def __str__(self) -> str:
        return f"{self.label}{self.operator}{self.value}"

    def alias(self, aliases: typing.Dict[str, str]) -> "Requirement":
        return dataclasses.replace(self, label=aliases.get(self.label, self.label))

    @classmethod
    def from_string(cls, string: str, regex: typing.Pattern) -> "Requirement":
        match = regex.match(string)
        if not match:
            raise KatieException(f"Could not parse selector requirement {string}")
        return cls(**match.groupdict())


@dataclasses.dataclass()
class Selector:
    regex = re.compile("(?P<label>.+)(?P<operator>=|==|!=)(?P<value>.+)")
    aliases = {}

    requirements: typing.Sequence[Requirement]

    def __str__(self) -> str:
        return ",".join(str(r) for r in self.requirements)

    def replace(self) -> "Selector":
        """Return the same selector with aliases applied."""
        return dataclasses.replace(
            self,
            requirements=[
                dataclasses.replace(r, label=self.aliases.get(r.label, r.label))
                for r in self.requirements
            ],
        )

    @staticmethod
    def as_strings(
        selector: typing.Optional["Selector"]
    ) -> typing.Sequence[typing.Optional[str]]:
        """Return the current selector and the selector with aliases applied as strings."""
        if selector is None:
            return [None]
        replace = selector.replace()
        if selector == replace:
            return [str(selector)]
        return [str(selector), str(replace)]

    @classmethod
    def from_string(cls, string: str) -> "Selector":
        return cls(
            requirements=[
                Requirement.from_string(s, cls.regex) for s in string.split(",")
            ]
        )


class FieldSelector(Selector):
    pass


class LabelSelector(Selector):
    aliases = {
        "name": "app.kubernetes.io/name",
        "instance": "app.kubernetes.io/instance",
        "version": "app.kubernetes.io/version",
        "component": "app.kubernetes.io/component",
        "part-of": "app.kubernetes.io/part-of",
        "managed-by": "app.kubernetes.io/managed-by",
    }


class SelectorType(click.ParamType):
    name = "selector"

    def __init__(self, cls: typing.Type[Selector] = LabelSelector):
        self.cls = cls

    def convert(
        self,
        value: str,
        param: typing.Optional[click.Parameter],
        ctx: typing.Optional[click.Context],
    ) -> Selector:
        return self.cls.from_string(value)
